﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson9
{
    public class Hero
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string NickName { get; set; }
        public int Level { get; set; }
        public virtual Weapon Weapon { get; set; }
    }
}
